<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//home


//user


Auth::routes();

Route::get('/', 'UserController@index')->name('home');
Route::post('/storeClock', 'UserController@storeClock')->name('user.storeClock');
//Route::group(['prefix'=>'user'],function() {
//
////    Route::group(['middleware'=>'auth:web'],function(){
////        Route::get('/', 'UserController@dashboard')->name('dashboard');
////
////
////
////    });
//
//}
//);
//admin
Route::group(['prefix' => 'admin'], function () {
    Route::group(['namespace' => 'Auth'], function () {
        Route::group(['middleware' => 'guest:admin'], function () {
            Route::get('/login', 'AdminLoginController@showLoginForm')->name('admin.login');
            Route::post('/login', 'AdminLoginController@login')->name('admin.login.submit');


            Route::get('/register', 'AdminRegisterController@showRegistrationForm')->name('admin.register');
            Route::post('/register', 'AdminRegisterController@register');

        });
        Route::post('/logout', 'AdminLoginController@logout')->name('admin.logout');


    });

    Route::group(['middleware' => 'auth:admin', 'namespace' => 'Admin'], function () {
        Route::get('/', 'AdminController@index')->name('admin.dashboard');

//        profile
        Route::group(['prefix' => 'profile'], function () {
            Route::get('/', 'ProfileController@index')->name('admin.profile');
            Route::post('/update/{id}', 'ProfileController@update')->name('admin.profile.update');
        });

        Route::group(['prefix' => 'employee'], function () {
            Route::get('/', 'EmployeeController@index')->name('employee.index');
            Route::get('/getEmployees', 'EmployeeController@getEmployees')->name('employee.getEmployees');
            Route::get('/{id}/edit', 'EmployeeController@edit')->name('employee.edit');
            Route::post('/store', 'EmployeeController@store')->name('employee.store');
            Route::post('/update/{id}', 'EmployeeController@update')->name('employee.update');
            Route::post('/delete/{id}', 'EmployeeController@delete')->name('employee.delete');
            Route::post('/resetPassword/{id}', 'EmployeeController@resetPassword')->name('employee.resetPassword');


            Route::get('/getCountries/', 'EmployeeController@getCountries')->name('employee.getCountries');
            Route::post('/getStates/', 'EmployeeController@getStates')->name('employee.getStates');


        });

        Route::group(['prefix' => 'timeSheet'], function () {
            Route::get('/', 'TimeSheetController@index')->name('timeSheet.index');
            Route::post('/getTimeSheets', 'TimeSheetController@getTimeSheets')->name('timeSheet.getTimeSheets');
            Route::get('/{id}/edit', 'TimeSheetController@edit')->name('timeSheet.edit');
            Route::post('/store', 'TimeSheetController@store')->name('timeSheet.store');
            Route::post('/update/{id}', 'TimeSheetController@update')->name('timeSheet.update');
            Route::post('/delete/{id}', 'TimeSheetController@delete')->name('timeSheet.delete');

        });

        Route::group(['prefix' => 'report'], function () {
            Route::get('/', 'ReportController@index')->name('report.index');
            Route::post('/getReports', 'ReportController@getReports')->name('report.getReports');

        });

        Route::group(['prefix' => 'holiday'], function () {
            Route::get('/', 'HolidayController@index')->name('holiday.index');
            Route::get('/getHolidays', 'HolidayController@getHolidays')->name('holiday.getHolidays');
            Route::get('/{id}/edit', 'HolidayController@edit')->name('holiday.edit');
            Route::post('/store', 'HolidayController@store')->name('holiday.store');
            Route::post('/update/{id}', 'HolidayController@update')->name('holiday.update');
            Route::post('/delete/{id}', 'HolidayController@delete')->name('holiday.delete');

        });

        Route::group(['prefix' => 'setting'], function () {
            Route::get('/', 'SettingController@index')->name('setting.index');
            Route::post('/store/{id?}', 'SettingController@store')->name('setting.store');


        });




    });


});
