var employeemodule=new Employee();
function  Employee(){
    var $self=this;
    var $pageName=$('#employee');
    var selector = '.context-menu-employee';
    var $formName = $('#employee_form');
    var csrf_token=$('meta[name="csrf-token"]').attr('content');
    var sharedModule=new Shared();

    //intialize the list-categories
    this.init=function(){

    //    get Categories;
        $self.getEmployees();
        $self.getAllCountries();
    }

    this.bind=function(){
        $formName.on('submit',function(e){

            e.preventDefault();
            sharedModule.removeValidationErrors($formName);
            var id=$(this).find('input[name="id"]').val();
            var formInput=$(this).serialize()+'&_token='+csrf_token;

            var buttonName=$formName.find('button[type="submit"]').text();

            if(id){
                if(buttonName=='Reset'){
                    $self.reset_password(formInput,id);
                }else{
                    $self.update(formInput,id);
                }

            }else{
                $self.store(formInput);
            }


        })

        $pageName.on('click','#addEmployeeBtn',function(){
            sharedModule.emptyForm($formName);
            sharedModule.removeValidationErrors($formName);
            $formName.find('#name,#email,#password,#password_confirmation').show();
            $pageName.find('.employee-form-page .box-title.action').html(' <strong>Add  Employee </strong>');
            $formName.find('button[type="submit"]').text('Created');

            //deactive the list status
            $self.checkListStatus();

            //    removing other information input
            $formName.find('#other_information_page').hide();
        })

    //    get States List of Country as selected

        $formName.on('change','select[name="country"]',function(){
                var countryName=$(this).val();
            console.log(countryName);
            $self.getStates(countryName);
        })



    }

    this.getEmployees=function(){
        $.get(base_url+'/admin/employee/getEmployees',function(resp){
           var employeeListHtml="";
            $.each(resp,function(i,v) {
                employeeListHtml += '<li ><a href="#" id="'+ v.id+'" class=" context-menu-employee"> ' + v.name + '<i class="fa fa-circle pull-right text-orange"></i></a></li>';
            });
            $pageName.find('#employee_list').html(employeeListHtml);

            //set context menu
            $(selector).contextMenu('employee-action', {

                bindings: {

                    'edit': function (selector) {
                        sharedModule.removeValidationErrors($formName);
                        var id = selector.id;
                        $self.edit(id);



                    },
                    'delete': function (selector) {
                        sharedModule.removeValidationErrors($formName);
                        var id = selector.id;
                        if(confirm('Are you sure want to remove ?')==true){
                            $self.delete(id);
                        }else{
                            return false;
                        }


                    },
                    'reset_password': function (selector) {
                        sharedModule.emptyForm($formName);

                        var id = selector.id;
                        $self.showResetPassword(id);

                    }


                }

            });


        })


    }
    this.store=function(formData){


        $.post(base_url+'/admin/employee/store/',formData,function(resp){
            if(resp.validation_errors){
                sharedModule.validation_errors($formName, resp.validation_errors);
            }else if(resp.error){
                sharedModule.alertMessage($pageName, 'error', resp.error)
            }else{
                sharedModule.alertMessage($pageName, 'success', resp.success)



                sharedModule.emptyForm($formName);

                $self.getEmployees();
            }

        })
    }
    this.update=function(formInput,id){
        var formData=formInput;
        $.post(base_url+'/admin/employee/update/'+id,formData,function(resp){
            if(resp.validation_errors){
                sharedModule.validation_errors($formName, resp.validation_errors);
            }else if(resp.error){
                sharedModule.alertMessage($pageName, 'error', resp.error)
            }else{
                sharedModule.alertMessage($pageName, 'success', resp.success)





                $self.getEmployees();
            }
        })
    }
    this.edit=function(id){
        $.get(base_url + '/admin/employee/' + id + '/edit', function (resp) {
            var data=resp;
          $formName.find('input[name="id"]').val(data.id);
            $formName.find('input[name="name"]').show().val(data.name);
            $formName.find('input[name="email"]').show().val(data.email);
            $formName.find('button[type="submit"]').text('Update');

            $formName.find('#password').hide();
            $formName.find('#password_confirmation').hide();
            $pageName.find('.employee-form-page .box-title.action').html(' <strong>Edit  Employee </strong> ('+ data.name+')');

            //set list active on edit
            $self.checkListStatus(id);

        //    showing other information input
            $formName.find('#other_information_page').show();

            if(data.bio_data){
                $formName.find('select[name="department"]').val(data.bio_data.department);
                $formName.find('select[name="country"]').val(data.bio_data.country);
                $formName.find('input[name="city"]').val(data.bio_data.city);
                $formName.find('input[name="street"]').val(data.bio_data.street);
                $formName.find('input[name="phone"]'). val(data.bio_data.phone);
                $formName.find('input[name="fax"]').val(data.bio_data.fax);


                $self.getStates(data.bio_data.country).done(function(){
                    $formName.find('select[name="state"]').val(data.bio_data.state);
                });


            }


        })
    }

    this.showResetPassword=function(id){
        $.get(base_url + '/admin/employee/' + id + '/edit', function (resp) {
            var data=resp;
            $formName.find('input[name="id"]').val(data.id);
            $formName.find('name').hide();
            $formName.find('#email').hide();
            $formName.find('button[type="submit"]').text('Reset');

            $formName.find('#password').show();
            $formName.find('3password_confirmation').show();
            $pageName.find('.employee-form-page .box-title.action').html(' <strong>Reset Password of Employee </strong>('+ data.name+ ')');

            //set list active on reset password
            $self.checkListStatus(id);


            //    removing other information input
            $formName.find('#other_information_page').hide();
        })


    }

    this.reset_password=function(formInput,id){
        var formData=formInput;
        $.post(base_url+'/admin/employee/resetPassword/'+id,formData,function(resp){
            if(resp.validation_errors){
                sharedModule.validation_errors($formName, resp.validation_errors);
            }else if(resp.error){
                sharedModule.alertMessage($pageName, 'error', resp.error)
            }else{
                sharedModule.alertMessage($pageName, 'success', resp.success)





                $self.getEmployees();
            }
        })
    }
    this.delete=function(id){
        var formData=$(this).serialize()+'&_token='+csrf_token;
        $.post(base_url + '/admin/employee/delete/' + id , formData,function (resp) {
            if(resp.error){
                sharedModule.alertMessage($pageName, 'error', resp.error)
            }else{
                sharedModule.alertMessage($pageName, 'success', resp.success)

                $self.getEmployees();
            }
        })
    }

    this.checkListStatus=function(id){
        $pageName.find('#employee_list li').removeClass('active');
        if(id){
            $pageName.find('#employee_list a#'+id).parent().addClass('active');
        }

    }
    this.getAllCountries=function(){

     $.ajax({
            'url': base_url+'/admin/employee/getCountries/',
             'type':'get',
             success:function(data){
                 var stateList="<option selected disabled>Select Country</option>";
                 $.each(data,function(i,v){
                     stateList +='<option value="'+v+'">'+v+'</option>';
                 })

                 $formName.find('select[name="country"]').html(stateList)
             }
         });
    }
    this.getStates=function(countryName){
        var formData={_token:csrf_token,countryName:countryName}

      return  $.ajax({
            'url': base_url+'/admin/employee/getStates/',
            'type':'post',
            data:formData,
            success:function(data){

                    var stateList="<option selected disabled>Select State</option>";
                    $.each(data,function(i,v){
                        stateList +='<option value="'+v+'">'+v+'</option>';
                    })

                    $formName.find('select[name="state"]').html(stateList)

            }
        })

    }


    $(function(){
        $self.init();
        $self.bind();
    })
}



