var timeSheet_module = new TimeSheet();
function TimeSheet() {
    var $self = this;
    var $pageName = $('#page_timeSheet');
    var $tableName = $('#timeSheet_table');
    var $modalName = $('#timeSheet_modal_form');
    var $formName = $('#timeSheet_form');
    var $formName2=$('#timeSheet_search_form');
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var dataTables = "";

    //Import from shared js
    var sharedModule = new Shared();


    this.bind = function () {
        //add content
        $pageName.on('click', '#add_timeSheet_btn', function () {
            sharedModule.emptyForm($formName);
            sharedModule.removeValidationErrors($formName);
            $modalName.modal('show');
            $modalName.find('.modal-title').text('Add  TimeSheet Page Form');
            $formName.find('select[name="user_id"]').attr('disabled',false);
            $formName.find('select[name="user_id"]').next('input').remove();
            $formName.find('button[type="submit"]').text('Created');


        })


        //submit
        $formName.on('submit', function (e) {
            e.preventDefault();
            sharedModule.removeValidationErrors($formName);
            var id = $(this).find('input[name="id"]').val();
            var formInput = $(this).serialize() + '&_token=' + csrf_token;

            if (id) {
                $self.update(formInput,id);
            } else {
                $self.store(formInput);
            }


        })

        $formName2.on('submit', function (e) {
            e.preventDefault();
            sharedModule.removeValidationErrors($formName2);

            var formInput = $(this).serialize() + '&_token=' + csrf_token;


                $self.getTimeSheets(formInput);



        })
        $formName2.on('click','#refresh_btn',function(){
            sharedModule.emptyForm($formName2);
        })


        //edit
        $tableName.on('click', '.edit_timeSheet_btn', function () {
            sharedModule.emptyForm($formName);
            sharedModule.removeValidationErrors($formName);
            var id = $(this).data('id');
            $modalName.modal('show');
            $self.edit(id);


        });

        //delete
        $tableName.on('click', '.delete_timeSheet_btn', function () {
            var id = $(this).data('id');
            if (confirm('Are You sure want to remove ?') == true) {
                $self.delete(id)
            } else {
                return false;
            }


        });


    }

    this.getTimeSheets = function (formInput) {
        var formData="";
        if(formInput){
            formData=formInput;
        }else{
            formData={_token:csrf_token};
        }
        $.post(base_url + '/admin/timeSheet/getTimeSheets', formData,function (resp) {


            if (resp.validation_errors) {
                sharedModule.validation_errors($formName2, resp.validation_errors);
            } else if (resp.error) {
                sharedModule.alertMessage($pageName, 'error', resp.error)
            } else {
                var contentListHtml = "";
                $.each(resp, function (i, v) {
                    var date = new Date(v.date);
                    var months = ["January", "February", "March", "April", "May", "June",
                        "July", "August", "September", "October", "November", "December"
                    ];
                    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                    contentListHtml += '<tr>' +
                        '<td>' + (i + 1) + '</td>' +
                        '<td>' + days[date.getDay()] +' '+ months[date.getMonth()] +' '+ date.getDate() + ' '+ date.getFullYear() +'</td>' +
                        '<td>' + v.user.name + '</td>' +

                        '<td>' + new Date(v.clock_in).toString("hh:mm:ss tt")+ '</td>';
                    if (v.clock_out == null || v.clock_out == "" || v.clock_out== v.clock_in) {
                        contentListHtml += '<td> No Yet</td>';
                        contentListHtml += '<td> -</td>';
                    } else {
                        contentListHtml += '<td>' +new Date(v.clock_out).toString("hh:mm:ss tt")+ '</td>';
                        contentListHtml += '<td>' + v.duration + '</td>';
                    }



                    contentListHtml +='<td>' +
                        '<a href="#" data-id="' + v.id + '" class=" edit_timeSheet_btn btn btn-primary"><i class="fa fa-edit"></i></a> &nbsp;' +
                        '<a href="#"  data-id="' + v.id + '" class=" delete_timeSheet_btn btn btn-danger"><i class="fa fa-trash"></i></a> ' +
                        '</td>' +

                        '</tr>';
                });
                if (dataTables != "") {
                    dataTables.destroy();
                }
                $pageName.find('tbody').html(contentListHtml);
                //se5t data tables
                dataTables = $tableName.DataTable();
            }



        })


    }
    this.store = function (formData) {

        $.post(base_url + '/admin/timeSheet/store', formData, function (resp) {
            if (resp.validation_errors) {
                sharedModule.validation_errors($formName, resp.validation_errors);
            } else if (resp.error) {
                sharedModule.alertMessage($pageName, 'error', resp.error)
            } else {
                sharedModule.alertMessage($pageName, 'success', resp.success);

                sharedModule.emptyForm($formName);

                $self.getTimeSheets();
                $modalName.modal('hide');
            }

        })
    }
    this.update = function (formInput, id) {
        var formData = formInput;
        $.post(base_url + '/admin/timeSheet/update/' + id, formData, function (resp) {
            if (resp.validation_errors) {
                sharedModule.validation_errors($formName, resp.validation_errors);
            } else if (resp.error) {
                sharedModule.alertMessage($pageName, 'error', resp.error)
            } else {
                sharedModule.alertMessage($pageName, 'success', resp.success)

                $self.getTimeSheets();
                $modalName.modal('hide');
            }
        })
    }
    this.edit = function (id) {
        $.get(base_url + '/admin/timeSheet/' + id + '/edit', function (resp) {

            var data = resp;
            $formName.find('input[name="id"]').val(data.id);
            $formName.find('select[name="user_id"]').attr('disabled',true).val(data.user_id);
            $formName.find('select[name="user_id"]').after('<input class="hidden" name="user_id" value="'+data.user_id+'">')


            var clock_in = new Date(data.clock_in)
            $formName.find('input[name="clock_in_date"]').val(clock_in.toString("yyyy-MM-dd"));
            $formName.find('input[name="clock_in_time"]').val(clock_in.toString("hh:mm tt"));


            if (data.clock_out) {

                var clock_out = new Date(data.clock_out);


                $formName.find('input[name="clock_out_date"]').val(clock_out.toString("yyyy-MM-dd"));

                if(data.clock_out==data.clock_in){
                    $formName.find('input[name="clock_out_time"]').val(new Date().toString("hh:mm tt"));

                }else{
                    $formName.find('input[name="clock_out_time"]').val(clock_out.toString("hh:mm tt"));
                }


            }



            $formName.find('button[type="submit"]').text('Update');
            $modalName.find('.modal-title').text('Edit  TimeSheet Page Form');

        })
    }
    this.delete = function (id) {
        var formData = $(this).serialize() + '&_token=' + csrf_token;
        $.post(base_url + '/admin/timeSheet/delete/' + id, formData, function (resp) {
            if (resp.error) {
                sharedModule.alertMessage($pageName, 'error', resp.error)
            } else {
                sharedModule.alertMessage($pageName, 'success', resp.success)

                $self.getTimeSheets();
            }
        })
    }

    this.employeeList = function () {
        var formData = $(this).serialize() + '&_token=' + csrf_token;
        $.get(base_url + '/admin/employee/getEmployees', function (resp) {


            resp.forEach(function (v, i) {
                $formName.find('select[name="user_id"]').append('<option value="' + v.id + '">' + v.name + '</option>');
                $formName2.find('select[name="user_id"]').append('<option value="' + v.id + '">' + v.name + '</option>');
            })


        });
    }


    //initilaize  function
    this.init = function () {


        $self.getTimeSheets();

        //    get Employees
        $self.employeeList();
        $self.bind();
    }();
}



