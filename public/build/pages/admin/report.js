var report_module = new Report();
function Report() {
    var $self = this;
    var $pageName = $('#page_report');
    var $tableName = $('#report_table');
    var $reportEmployeePage = $('#employee-report');

    var dataTables = "";
    var $formTimelineReportName = $('#timeline_report_form');
    var $formMonthlyReportName = $('#monthly_report_form');
    var $formWeeklyReportName = $('#weekly_report_form');
    var csrf_token = $('meta[name="csrf-token"]').attr('content');

    var months = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    //pdf generate variable declaration
    var $reportPdfEmployeePage = $('#employee-pdf-report');
    var cache_width = $reportPdfEmployeePage.width(),
        a4 = [595.28, 841.89];  // for a4 size paper width and height


    //Import from shared js
    var sharedModule = new Shared();
    var user_id;


    this.bind = function () {
        //hide table
        $reportEmployeePage.hide(function () {
            $pageName.find('#generate_pdf').hide();

        });


        $formTimelineReportName.on('submit', function (e) {
            e.preventDefault();
            sharedModule.removeValidationErrors($formTimelineReportName);
            var formInput = $(this).serialize() + '&_token=' + csrf_token;


            $self.getReports(formInput, $formTimelineReportName);
        })
        $formMonthlyReportName.on('submit', function (e) {
            e.preventDefault();
            sharedModule.removeValidationErrors($formMonthlyReportName);
            var formInput = $(this).serialize() + '&_token=' + csrf_token;


            $self.getReports(formInput, $formMonthlyReportName);
        })
        $formWeeklyReportName.on('submit', function (e) {
            e.preventDefault();
            sharedModule.removeValidationErrors($formWeeklyReportName);
            var formInput = $(this).serialize() + '&_token=' + csrf_token;


            $self.getReports(formInput, $(this));
        })

        $formWeeklyReportName.on('change', 'input[name="year"],input[name="month"]', function () {

            var year, month;
            year = $formWeeklyReportName.find('input[name="year"]').val();
            month = $formWeeklyReportName.find('input[name="month"]').val();
            $self.getWeekFromDate(year, month);
        })

        $tableName.on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var type = tr.data('type');
            var date = tr.data('date');
            if (tr.hasClass('shown')) {
                $('tr.child[data-date="' + date + '"]').hide(function () {
                    tr.removeClass('shown');
                }).slideUp(3000);
            } else {
                $('tr.child[data-date="' + date + '"]').show(function () {
                    tr.addClass('shown');
                }).slideDown(3000);
            }


        })


        //    create Pdf
        $pageName.find('#generate_pdf').click(function () {
            var name = $(this).attr('name');
            var date = $(this).attr('date')

            $self.createPDF(name, date);
        })


    }

    this.setDataTable = function () {
        dataTables = $tableName.DataTable();
    }

    this.getReports = function (formData, formName) {

        $.post(base_url + '/admin/report/getReports', formData, function (resp) {
            if (resp.user) {
                user_id = resp.user.id
            }


            if (resp.validation_errors) {
                sharedModule.validation_errors(formName, resp.validation_errors);
            } else if (resp.error) {
                sharedModule.alertMessage($pageName, 'error', resp.error)
            } else {
                var contentListHtml = "";
                var total_hrs = 0;
                $.each(resp.user_reports, function (i, v) {
                    var date = new Date(v.date);

                    if (v.status == 'working') {

                        total_hrs += v.total_working_hours;

                        contentListHtml += '<tr data-type="working" data-date="' + v.date + '">' +
                            '<td class="details-control">&nbsp;</td>' +

                            '<td>' + days[date.getDay()] + ' ' + months[date.getMonth()] + ' ' + date.getDate() + ' ' + date.getFullYear() + '</td>' +

                            '<td> - </td>' +
                            '<td>' + v.working_hour + '</td>' +
                            '</tr>';
                    }

                    if (v.status == 'holiday') {
                        contentListHtml += '<tr >' +
                            '<td>&nbsp;</td>' +
                            '<td>' + days[date.getDay()] + ' ' + months[date.getMonth()] + ' ' + date.getDate() + ' ' + date.getFullYear() + '</td>' +

                            '<td>' + v.type + '</td>' +
                            '<td> - </td>' +

                            '</tr>';
                    }
                    if (v.timeSheet) {
                        $.each(v.timeSheet, function (i, val) {
                            var clock_in = new Date(val.clock_in);
                            var clock_out = new Date(val.clock_out);
                            contentListHtml += '<tr data-date="' + v.date + '" class="child" style="display: none">' +

                                '<td >&nbsp;</td>' +
                                '<td >' +
                                '<span>' + (i + 1) + '</span>' +
                                '<span class="text-success">clock In :&nbsp;' + clock_in.toString("hh:mm tt") + '</span>' +
                                '<span class="text-danger">clock Out:&nbsp;' + clock_out.toString("hh:mm tt") + '</span>' +
                                '</td>' +
                                '<td >&nbsp;</td>' +
                                '<td >&nbsp;</td>' +
                                '</tr>';
                        });
                    }


                });
                if (dataTables != "") {
                    dataTables.destroy();
                }
                $pageName.find('tbody').html(contentListHtml);
                $pageName.find('#generate_pdf').attr({'name': resp.user.name, 'date': resp.report_date})
                if (resp.total_working_hours || resp.total_working_hours != "0:0") {
                    $pageName.find('tfoot').show();
                    $pageName.find('.total_hrs').text(resp.total_working_hours);
                } else {
                    $pageName.find('tfoot').hide();
                }


                //se5t data tables
                dataTables = $tableName.DataTable();

                $reportEmployeePage.show('slide', {direction: "left"}, function () {
                    $pageName.find('#generate_pdf').show();

                }, 1000);
                //set header
                var reportHeaderHtml = '   <div class="row report-header">' +
                    '<div class="col-md-12 text-center">' +
                    '<h3><strong>Codeaxis Employee Report </strong><small class="pull-right"><strong>Report Date:&nbsp;</strong>' + resp.report_date + '</small></h3>' +

                    '</div>' +
                    '<div class="col-md-6">' +
                    '<ul  class="pull-left list-unstyled">' +
                    '<li><strong>From Date:&nbsp;</strong>' + resp.from_date + '</li>' +
                    '<li><strong> Name:&nbsp;</strong>' + resp.user.name + '</li>' +
                    '</ul>' +
                    '</div>' +

                    '<div class="col-md-6">' +
                    '<ul class="pull-right list-unstyled" >' +
                    '<li><strong>To Date:&nbsp;</strong>' + resp.to_date + '</li>' +

                    '</ul>' +
                    '</div>' +

                    '</div>';
                $pageName.find('.report-header').remove();
                $reportPdfEmployeePage.prepend(reportHeaderHtml);
                $reportEmployeePage.find('.dataTables_wrapper div.row:nth(0)').after(reportHeaderHtml);
            }


        })


    }

    //get Employee list
    this.employeeList = function () {
        var formData = $(this).serialize() + '&_token=' + csrf_token;
        $.get(base_url + '/admin/employee/getEmployees', function (resp) {


            resp.forEach(function (v, i) {
                $formTimelineReportName.find('select[name="user_id"]').append('<option value="' + v.id + '">' + v.name + '</option>');
                $formMonthlyReportName.find('select[name="user_id"]').append('<option value="' + v.id + '">' + v.name + '</option>');
                $formWeeklyReportName.find('select[name="user_id"]').append('<option value="' + v.id + '">' + v.name + '</option>');

            })


        });
    }
    this.getWeekFromDate = function (year, month) {
        function endFirstWeek(firstDate, firstDay) {
            if (!firstDay) {
                return 7 - firstDate.getDay();
            }
            if (firstDate.getDay() < firstDay) {
                return firstDay - firstDate.getDay();
            } else {
                return 7 - firstDate.getDay() + firstDay;
            }
        }


        var weeks = [],
            firstDate = new Date(year, month, 1),
            lastDate = new Date(year, month + 1, 0),
            numDays = lastDate.getDate();

        var start = 1;
        var end = endFirstWeek(firstDate, 6);
        while (start <= numDays) {
            weeks.push({start: start, end: end});
            start = end + 1;
            end = end + 7;
            end = start === 1 && end === 8 ? 1 : end;
            if (end > numDays) {
                end = numDays;
            }
        }


        var weekHtmlList = "";
        $.each(weeks, function (i, v) {
            weekHtmlList += '<option value="' + v.start + '-' + v.end + '"> Week &nbsp;' + (i + 1) + '(' + v.start + '-' + v.end + ')</option>';
        })

        $formWeeklyReportName.find('select[name="week"]').html(weekHtmlList);


    }


    //Convert Html to Pdf

    this.createPDF = function (name, date) {
        $self.getCanvas().then(function (canvas) {
            var
                img = canvas.toDataURL("image/png"),
                doc = new jsPDF({
                    unit: 'px',
                    format: 'a4'
                });
            doc.addImage(img, 'JPEG', 20, 20);
            doc.save(name + '-' + date + 'pdf');
            $reportPdfEmployeePage.css({'display': 'none'});
        });
    }
    this.getCanvas = function () {
        $reportPdfEmployeePage.width((a4[0] * 1.33333) - 80).css({'max-width': 'none', 'display': 'block'});
        return html2canvas($reportPdfEmployeePage, {
            imageTimeout: 2000,
            removeContainer: true
        });
    }

    //end of convert Html to pdf

    //initilaize  function
    this.init = function () {

        $self.employeeList();
        $self.bind();

    }();
}



