var holiday_module = new Holiday();
function Holiday() {
    var $self = this;
    var $pageName = $('#page_holiday');
    var $tableName = $('#holiday_table');
    var $modalName = $('#holiday_modal_form');
    var $formName = $('#holiday_form');


    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var dataTables = "";

    //Import from shared js
    var sharedModule = new Shared();


    this.bind = function () {
        //add content
        $pageName.on('click', '#add_holiday_btn', function () {
            sharedModule.emptyForm($formName);
            sharedModule.removeValidationErrors($formName);
            $modalName.modal('show');
            $modalName.find('.modal-title').text('Add  Holiday Page Form');
            $formName.find('select[name="user_id"]').attr('disabled', false);
            $formName.find('select[name="user_id"]').next('input').remove();
            $formName.find('button[type="submit"]').text('Save');


        })


        //submit
        $formName.on('submit', function (e) {
            e.preventDefault();
            sharedModule.removeValidationErrors($formName);
            var id = $(this).find('input[name="id"]').val();
            var formInput = $(this).serialize() + '&_token=' + csrf_token;

            if (id) {
                $self.update(formInput, id);
            } else {
                $self.store(formInput);
            }


        })


        //edit
        $tableName.on('click', '.edit_holiday_btn', function () {
            sharedModule.emptyForm($formName);
            sharedModule.removeValidationErrors($formName);
            var id = $(this).data('id');
            $modalName.modal('show');
            $self.edit(id);


        });

        //delete
        $tableName.on('click', '.delete_holiday_btn', function () {
            var id = $(this).data('id');
            if (confirm('Are You sure want to remove ?') == true) {
                $self.delete(id)
            } else {
                return false;
            }


        });


    }

    this.getHolidays = function (formInput) {
        var formData = "";
        if (formInput) {
            formData = formInput;
        } else {
            formData = {_token: csrf_token};
        }
        $.get(base_url + '/admin/holiday/getHolidays', function (resp) {
            var contentListHtml = "";

            $.each(resp, function (i, v) {
                var date = new Date(v.date);
                var months = ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"
                ];
                var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                contentListHtml += '<tr>' +
                    '<td>' + (i + 1) + '</td>' +
                    '<td>' + days[date.getDay()] + ' , ' + months[date.getMonth()] + ' ' + date.getDate() + ' , ' + date.getFullYear() + '</td>' +
                    '<td>' + v.user.name + '</td>' +
                    '<td>' + v.type + '</td>' +
                    '<td>' +
                    '<a href="#" data-id="' + v.id + '" class=" edit_holiday_btn btn btn-primary"><i class="fa fa-edit"></i></a> &nbsp;' +
                    '<a href="#"  data-id="' + v.id + '" class=" delete_holiday_btn btn btn-danger"><i class="fa fa-trash"></i></a> ' +
                    '</td>' +

                    '</tr>';
            });
            if (dataTables != "") {
                dataTables.destroy();
            }
            $pageName.find('tbody').html(contentListHtml);
            //se5t data tables
            dataTables = $tableName.DataTable();


        })


    }
    this.store = function (formData) {

        $.post(base_url + '/admin/holiday/store', formData, function (resp) {
            if (resp.validation_errors) {
                sharedModule.validation_errors($formName, resp.validation_errors);
            } else if (resp.error) {
                sharedModule.alertMessage($pageName, 'error', resp.error)
            } else {
                sharedModule.alertMessage($pageName, 'success', resp.success);

                sharedModule.emptyForm($formName);

                $self.getHolidays();
                $modalName.modal('hide');
            }

        })
    }
    this.update = function (formInput, id) {
        var formData = formInput;
        $.post(base_url + '/admin/holiday/update/' + id, formData, function (resp) {
            if (resp.validation_errors) {
                sharedModule.validation_errors($formName, resp.validation_errors);
            } else if (resp.error) {
                sharedModule.alertMessage($pageName, 'error', resp.error)
            } else {
                sharedModule.alertMessage($pageName, 'success', resp.success)

                $self.getHolidays();
                $modalName.modal('hide');
            }
        })
    }
    this.edit = function (id) {
        $.get(base_url + '/admin/holiday/' + id + '/edit', function (resp) {

            var data = resp;
            $formName.find('input[name="id"]').val(data.id);
            $formName.find('select[name="user_id"]').attr('disabled', true).val(data.user_id);
            $formName.find('select[name="user_id"]').after('<input class="hidden" name="user_id" value="' + data.user_id + '">')
            $formName.find('select[name="type"]').val(data.type);

            $formName.find('input[name="date"]').val(data.date);
            $formName.find('textarea[name="note"]').text(data.note);

            $formName.find('button[type="submit"]').text('Update');
            $modalName.find('.modal-title').text('Edit  Holiday Page Form');

        })
    }
    this.delete = function (id) {
        var formData = $(this).serialize() + '&_token=' + csrf_token;
        $.post(base_url + '/admin/holiday/delete/' + id, formData, function (resp) {
            if (resp.error) {
                sharedModule.alertMessage($pageName, 'error', resp.error)
            } else {
                sharedModule.alertMessage($pageName, 'success', resp.success)

                $self.getHolidays();
            }
        })
    }

    this.employeeList = function () {
        var formData = $(this).serialize() + '&_token=' + csrf_token;
        $.get(base_url + '/admin/employee/getEmployees', function (resp) {


            resp.forEach(function (v, i) {
                $formName.find('select[name="user_id"]').append('<option value="' + v.id + '">' + v.name + '</option>');


            })


        });
    }


    //initilaize  function
    this.init = function () {


        $self.getHolidays();

        //    get Employees
        $self.employeeList();
        $self.bind();
    }();
}



