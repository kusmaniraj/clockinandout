<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{


 protected   $table="holidays";
    protected $fillable=['id','user_id','type','note','date'];
    public function User(){
        return $this->belongsTo('App\User','user_id','id');
    }

}
