<?php

namespace App\Console\Commands;

use App\Clock;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ClockedOutUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clocked_out:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Successfully Set the Clock Out TimeStamp of User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Request $request)
    {
       $userTimeSheets=Clock::with('User')->where(['clock_out'=>null])->get();

        if($userTimeSheets){
            foreach($userTimeSheets as $userTimeSheet){
                $clock_out=now()->format('Y-m-d'). ' '.now()->format('18:00:00');
                Clock::find($userTimeSheet['id'])->update(['clock_out'=>$clock_out]);


            }
        }

    }
}
