<?php namespace App\Traits;

use App\Clock;
use App\Holiday;
use Carbon\Carbon;
trait ClockValidation
{
    public function checkHoliday($user_id, $date)
    {


        $holidays = Holiday::where(['user_id' => $user_id])->whereDate('date', $date)->first();
        if ($holidays) {
            return true;
        } else {
            return false;
        }

    }

    public function checkClockOutDate($user_id, $date)
    {
        $previousClocked = Clock::where(['user_id' => $user_id])->whereDate('date', $date)->orderBy('id', 'desc')->first();
        if ($previousClocked && $previousClocked['clock_out'] == $previousClocked['clock_in']) {

            return true;
        } else {
            return false;
        }
    }

    public function checkClockInDate($user_id, $date){
        $previousClocked = Clock::where(['user_id' =>$user_id])->whereDate('date',$date)->orderBy('id', 'desc')->first();
        if ($previousClocked==false || $previousClocked['clock_out'] != $previousClocked['clock_in']) {
         return true;
        }else{
            return false;        }
    }

    public function checkClockOutTime($user_id, $date, $clock_in_time=null)
    {
        $previousClocked = Clock::where(['user_id' => $user_id])->whereDate('date', $date)->orderBy('id', 'desc')->first();
        if($clock_in_time==null){
            $clock_in_time=  Carbon::now()->format('H:i');
        }

        $clocked_out_time=Carbon::parse($previousClocked['clock_out']);;
        if ($previousClocked && $clock_in_time <= Carbon::parse($clocked_out_time)->format('H:i')) {

           return true;

        } else {
            return false;
        }
    }
}
