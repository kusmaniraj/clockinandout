<?php namespace App\Traits;
use PragmaRX\Countries\Package\Countries;


trait CountriesAndStates
{


    public function getAllCountries(){
        return Countries::all()->pluck('name.common');
    }
    public function getStatesOfCountry($countryName){
      return Countries::where('name.common', $countryName)->first()->hydrateStates()->states->sortBy('name')->pluck('name');
    }
}
