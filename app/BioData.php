<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BioData extends Model
{
    protected $table='biodatas';
    protected $fillable=['user_id','phone','city','street','department','country','state','fax'];
}
