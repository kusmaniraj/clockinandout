<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clock extends Model
{
    protected $table='clocks';
    protected $fillable=['id','user_id','clock_in','clock_out','date'];
    public function User(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
