<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table='settings';
    protected $fillable=['id','company_name','company_short_name','company_slogan','email','address','phone_number','fax_number','company_version','company_version','company_start_date'];
}
