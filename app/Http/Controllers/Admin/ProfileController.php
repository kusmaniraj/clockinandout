<?php

namespace App\Http\Controllers\Admin;


use App\Admin;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class ProfileController extends Controller
{
    private $data;

    public function __construct()
    {
        $this->data['title']='Profile';




    }
    public function index(){
        if (Auth::guard('admin')->check()) {
            $this->data['admin']=Auth::user();
        }

        return view('admin.profile')->with($this->data);
    }
    public function update(Request $request,$id){
        $validator=Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,id,'.$id,
            'old_password'=>'required',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if($validator->fails()){

            return redirect()->back()->withErrors($validator);
        }



        if(Hash::check($request->old_password,Auth::user()->password)==false){


            $validator->errors()->add('old_password', 'Old Password Not Match');
            return redirect()->back()->withErrors($validator);
        }else{

            $result=Admin::find($id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            if($result){
                $request->session()->flash('success','Profile has been Updated');
            }else{
                $request->session()->flash('error','Cannot Updated Profile  ');
            }
            return redirect()->back();
        }


    }
}
