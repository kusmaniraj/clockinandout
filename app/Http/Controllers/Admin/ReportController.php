<?php

namespace App\Http\Controllers\Admin;

use App\Clock;
use App\Holiday;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Validator;

class ReportController extends Controller
{
    private $data;

    public function __construct()
    {
        $this->data['title'] = 'Reports';


    }

    public function index()
    {

        return view('admin.report.list', $this->data);
    }

    public function getReports(Request $request)
    {
        $data['user_reports'] = [];
        $totalClocks = [];
        $holidays = [];
        $workingDays = [];
        $data['total_working_hours'] = 0;


        if ($request->reportType == "timeline") {
//           timeline validation
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',


            ], [
                'user_id.required' => ' Employee Field is required',


            ]);
            if ($request->start_date > $request->end_date) {
                return ['validation_errors' => ['Start Date Must be less or equal to End Date']];
            }
            if ($validator->fails()) {
                return response()->json(['validation_errors' => $validator->errors()]);
            }
            if ($timeline = $this->getTimelineSheet($request->user_id, $request->start_date, $request->end_date)) {
                $workingDays = $timeline['workingDays'];
                $holidays = $timeline['holidays'];
            }


        } elseif($request->reportType == "monthly"){

//            month validation

            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'year' => 'required',
                'month' => 'required',


            ], [
                'user_id.required' => ' Employee Field is required',
            ]);
            if ($validator->fails()) {
                return response()->json(['validation_errors' => $validator->errors()]);
            }

            if ($monthly = $this->getMonthlySheet($request->user_id, $request->year, $request->month)) {
                $workingDays = $monthly['workingDays'];
                $holidays = $monthly['holidays'];
            }


        }else{
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'year' => 'required',
                'month' => 'required',
                'week' => 'required',


            ], [
                'user_id.required' => ' Employee Field is required',
            ]);
            if ($validator->fails()) {
                return response()->json(['validation_errors' => $validator->errors()]);
            }
            $week=explode('-',$request->week);
            $start_date=$request->year.'-'.$request->month.'-'.$week[0];
            $end_date=$request->year.'-'.$request->month.'-'.$week[1];


            if ($monthly = $this->getWeeklySheet($request->user_id, $start_date, $end_date)) {
                $workingDays = $monthly['workingDays'];
                $holidays = $monthly['holidays'];
            }

        }

//working Time /calculate duration
        if ($workingDays) {
            $data['total_working_hours'] = $this->calculateTotalWorkingHours($workingDays);

        }

//get report Sheet
        $data['user_reports'] = $this->getReportsSheet($request->user_id, $workingDays, $holidays);


//        return Data


        $data['user'] = User::find($request->user_id);
        if ($request->reportType == "timeline") {
            $data['from_date'] = $request->start_date;
            $data['to_date'] = $request->end_date;
        } else {
            $data['from_date'] = $request->year . '-' . $request->month . '-1';
            $lastDay = date('t', strtotime($request->month));
            $data['to_date'] = $request->year . '-' . $request->month . '-' . $lastDay;


        }
        $data['report_date'] = Carbon::now()->format('Y-m-d');

        return $data;

    }


    public function getTimelineSheet($user_id, $start_date,$end_date )
    {
        //            end of time line validation
        $timeSheets = Clock::where(['user_id' => $user_id])->whereDate('date', '<=', Carbon::parse($end_date))->whereDate('date', '>=', Carbon::parse($start_date))->get()->groupBy('date');
        $data['holidays'] = Holiday::where(['user_id' => $user_id])->whereDate('date', '<=', Carbon::parse($end_date))->whereDate('date', '>=', Carbon::parse($start_date))->get();
        $data['workingDays'] = $this->groupByDate($timeSheets);

        return $data;


    }

    public function getMonthlySheet($user_id, $year, $month)
    {


        $timeSheets = Clock::where(['user_id' => $user_id])->whereYear('date', '=', $year)->whereMonth('date', '=', $month)->get()->groupBy('date');
        $data['holidays'] = Holiday::where(['user_id' => $user_id])->whereYear('date', '=', $year)->whereMonth('date', '=', $month)->get();
        $data['workingDays'] = $this->groupByDate($timeSheets);
        return $data;


    }
    public function getWeeklySheet($user_id, $start_date,$end_date )
    {
        //            end of time line validation
        $timeSheets = Clock::where(['user_id' => $user_id])->whereDate('date', '<=', Carbon::parse($end_date))->whereDate('date', '>=', Carbon::parse($start_date))->get()->groupBy('date');
        $data['holidays'] = Holiday::where(['user_id' => $user_id])->whereDate('date', '<=', Carbon::parse($end_date))->whereDate('date', '>=', Carbon::parse($start_date))->get();
        $data['workingDays'] = $this->groupByDate($timeSheets);

        return $data;


    }

    public function groupByDate($timeSheets)
    {
        $workingDays = [];
        if ($timeSheets) {
            foreach ($timeSheets as $date => $timeSheet) {
                $working_hour = 0;


                foreach ($timeSheet as $value) {
                    $clock_in = Carbon::parse($value->clock_in);
                    $clock_out = Carbon::parse($value->clock_out);


                    $working_hour += floor($clock_out->diff($clock_in)->format('%I')) + ($clock_out->diff($clock_in)->format('%H') * 60);
                }

                array_push($workingDays, ['date' => $date, 'working_hour' => $working_hour]);


            }
        }

        return $workingDays;
    }

    public function calculateTotalWorkingHours($workingDays)
    {
        $total_min = 0;
        foreach ($workingDays as $key => $time) {
            $total_min += $time['working_hour'];
        }
        return floor($total_min / 60) . ' hrs :' . ($total_min - floor($total_min / 60) * 60) . ' min';
    }

    public function getReportsSheet($user_id, $workingDays,$holidays)
    {
        $newSheetArray = [];
        //        push holidays day in the report

        if($holidays){
            foreach ($holidays as $key => $holiday) {
                array_push($newSheetArray, ['date' => $holiday->date, 'type' => $holiday->type, 'user_id' => $holiday->user_id, 'note' => $holiday->note, 'status' => 'holiday']);
            }
        }

        if($workingDays){
            //        push working day in the report
            foreach ($workingDays as $key => $clock) {

                $working_hour = floor($clock['working_hour'] / 60) . ' hrs :' . ($clock['working_hour'] - floor($clock['working_hour'] / 60) * 60) . ' min';  //set format to time

                array_push($newSheetArray, ['date' => $clock['date'], 'working_hour' => $working_hour, 'status' => 'working', 'timeSheet' => Clock::where(['user_id' => $user_id, 'date' => $clock['date']])->get()]);


            }
        }



        return $newSheetArray;
    }
}
