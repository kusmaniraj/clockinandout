<?php

namespace App\Http\Controllers\Admin;

use App\BioData;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use App\Traits\CountriesAndStates;

class EmployeeController extends Controller
{
    protected $data;
    use CountriesAndStates;
    public function __construct()
    {
        $this->data['title']='Employee';
    }

    public function index(){
        $this->data['users']=User::all();
        $this->data['countries']=$this->getCountries(); //call function from CountriesAndState Trait

        return view('admin.employee.list',$this->data);
    }
    public  function getEmployees(){
        return User::all();
    }
    public function store(Request $request){
        $validator=Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if($validator->fails()){

            return ['validation_errors'=>$validator->errors()];
        }


        $result=User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        if($result){
           return['success'=>'Employee has been Added'];
        }else{
            return['error'=>'Cannot added Employee '];
        }

    }

    public function edit($id){

        return User::where('id',$id)->with('bioData')->first();
    }

    public function update(Request $request,$id){
        $validator=Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,id,'.$id,
            'department' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'street' => 'required',



        ]);
        if($validator->fails()){

            return ['validation_errors'=>$validator->errors()];
        }


        $result=User::find($id)->update([
            'name' => $request->name,
            'email' => $request->email,

        ]);
        $otherInformationData=[
            'user_id'=>$id,
            'country'=>$request->country,
            'state'=>$request->state,
            'city'=>$request->city,
            'street'=>$request->street,
            'phone'=>$request->phone,
            'department'=>$request->department,
            'fax'=>$request->fax,

        ];
       if($this->storeOtherInformation($otherInformationData)==false){
           return['error'=>'Cannot Update Other Information'];
       }
        if($result){
            return['success'=>'Employee had been Updated'];
        }else{
            return['error'=>'Cannot updated Employee '];
        }


    }
    public function resetPassword(Request $request,$id){
        $validator=Validator::make($request->all(),[

            'password' => 'required|string|min:6|confirmed',
        ]);
        if($validator->fails()){

            return ['validation_errors'=>$validator->errors()];
        }




        $result=User::find($id)->update([
            'password' => Hash::make($request->password)

        ]);

        if($result){
            return['success'=>'Employee Password has been changed'];
        }else{
            return['error'=>'Cannot Changed Employee Password '];
        }
    }
    public function delete($id){
        $result=User::find($id)->delete();
        if($result){
            return['success'=>'Employee had been Updated'];
        }else{
            return['error'=>'Cannot updated Employee '];
        }
    }
    public function getCountries(){
        return $this->getAllCountries();  //call function from CountriesAndState Trait
    }
    public function getStates(Request $request){
        return $this->getStatesOfCountry($request->countryName);  //call function from CountriesAndState Trait
    }

    public function storeOtherInformation($formData){
        $result=BioData::updateOrCreate(['user_id'=>$formData['user_id']],$formData);
        if($result){
            return true;
        }else{
            return false;
        }
    }
}
