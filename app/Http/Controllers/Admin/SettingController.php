<?php

namespace App\Http\Controllers\Admin;


use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;


class SettingController extends Controller
{
    private $data;

    public function __construct()
    {
        $this->data['title'] = 'Setting';


    }

    public function index()
    {


        $this->data['getSetting'] = Setting::find(1);
        return view('admin.setting')->with($this->data);
    }

    public function store(Request $request, $id = null)
    {
        $this->validate($request, [
            'company_name' => 'required',
            'company_short_name' => 'required',
            'company_slogan' => 'required',
            'company_version' => 'required|regex:/^[0-9.]+$/',
            'company_start_date' => 'required',
            'email' => 'required',
            'address' => 'required',
            'phone_number' => 'required',

        ]);
        $formInputs = $request->all();

        if ($id) {
            $this->update($formInputs, $id);

        } else {
            $this->insert($formInputs);
        }
        return redirect()->back();
    }

    public function insert($formInputs)
    {
        $result = Setting::create($formInputs);
        if ($result) {
            Session::flash('success', 'Created Setting Successfully');
        } else {
            Session::flash('error', ' Cannot Created Setting ');
        }

    }

    public function update($formInputs, $id)
    {
        $result = Setting::find($id)->update($formInputs);
        if ($result) {
            Session::flash('success', 'Updated Setting Successfully');
        } else {
            Session::flash('error', 'Cannot Update Setting ');
        }

    }

}
