<?php

namespace App\Http\Controllers\Admin;

use App\Clock;
use App\Holiday;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Validator;
use DB;
use App\Traits\ClockValidation;

class TimeSheetController extends Controller
{
    use ClockValidation;
    private $data;
    protected $rule = [
        'user_id' => 'required',
        'clock_in_date' => 'required',
        'clock_in_time' => 'required',


    ];
    protected $message = [
        'user_id.required' => ' Employee Field is required',


    ];

    public function __construct()
    {
        $this->data['title'] = 'TimeSheet';


    }

    public function index()
    {

        return view('admin.timeSheet.list', $this->data);
    }

    public function getTimeSheets(Request $request)
    {

        if ($request->has('user_id')) {
            $validator = Validator::make($request->all(), ['start_date' => 'required', 'end_date' => 'required']);
            if ($validator->fails()) {
                return response()->json(['validation_errors' => $validator->errors()]);
            }
            if ($request->start_date >= $request->end_date) {
                return ['validation_errors' => ['Start Date Must be less or equal to End Date']];
            }
            if ($request->user_id == 0) {
                $usersTimeSheet = Clock::with('User')->whereDate('date', '<=', $request->end_date)->whereDate('date', '>=', $request->start_date)->get();
            } else {
                $usersTimeSheet = Clock::with('User')->where(['user_id' => $request->user_id])->whereDate('date', '<=', $request->end_date)->whereDate('date', '>=', $request->start_date)->get();
            }

        } else {
            $usersTimeSheet = Clock::with('User')->whereDate('date', now()->format('Y-m-d'))->get();
        }

        foreach ($usersTimeSheet as $key => $time) {

            $startTime = Carbon::parse($time->clock_in);
            if ($time->clock_out) {
                $finishTime = Carbon::parse($time->clock_out);
            } else {
                $finishTime = Carbon::parse(now());
            }

            $usersTimeSheet[$key]['duration'] = $finishTime->diff($startTime)->format('%H:%I:%S');
        }

        return $usersTimeSheet;

    }

    public function store(Request $request)
    {


        $validator = Validator::make($request->all(), $this->rule, $this->message);
        if ($validator->fails()) {
            return response()->json(['validation_errors' => $validator->errors()]);
        }

        //        check holidays or not
        if ($result = $this->checkHoliday($request->user_id, $request->clock_in_date)) {
            return response()->json(['validation_errors' => ['clock_in' => 'Holiday is already set in Selected Date ' . Carbon::parse($request->clock_in_date)->format('Y-m-d')]]);
        }


        if ($request->clock_out_date && $request->clock_out_time) {
            if ($result = $this->clockValidation($request->clock_in_date, $request->clock_in_time, $request->clock_out_date, $request->clock_out_time)) {
                return response()->json($result);
            } else {
                $clockedOutTimeStamp = $request->clock_out_date . ' ' . Carbon::parse($request->clock_out_time)->format('H:i');
                $formData['clock_out'] = $clockedOutTimeStamp;

            }

        }


        if ($this->checkClockOutTime($request->user_id, $request->clock_in_date, Carbon::parse($request->clock_out_time)->format('H:i'))) {
            return response()->json(['validation_errors' => ['clock_in' => 'Clocked In Time must be after previous clocked out user on today Date: ' . Carbon::parse($request->clock_in_date)->format('Y-m-d')]]);
        }
        if ($this->checkClockOutDate($request->user_id, $request->clock_in_date)) {
            return response()->json(['validation_errors' => ['clock_out' => 'Selected User had not clocked out yet']]);
        }



        $clockedInTimeStamp = $request->clock_in_date . ' ' . Carbon::parse($request->clock_in_time)->format('H:i');//set format into timestamp
        $formData = [

            'clock_in' => $clockedInTimeStamp,
            'clock_out' => $clockedInTimeStamp,
            'user_id' => $request->user_id,
            'date' => $request->clock_in_date
        ];
//        store TimeSheet
        $result = Clock::create($formData);
        if ($result) {
            return ['success' => 'Created the  TimeSheet'];
        } else {
            return ['error' => ' Cannot Created the  TimeSheet'];
        }
    }

    public function edit($id)
    {
        return Clock::find($id);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rule, $this->message);
        if ($validator->fails()) {
            return response()->json(['validation_errors' => $validator->errors()]);
        }
        //        check holidays or not using Trait
        if ($result = $this->checkHoliday($request->user_id, $request->clock_in_date)) {
            return response()->json(['validation_errors' => ['clock_in' => 'Holiday is already set in Selected Date ' . Carbon::parse($request->clock_in_date)->format('Y-m-d')]]);

        }


        $clockedInTimeStamp = $request->clock_in_date . ' ' . Carbon::parse($request->clock_in_time)->format('H:i'); //set format into timestamp
        if ($request->clock_out_date && $request->clock_out_time) {
            if ($result = $this->clockFormValidation($request->clock_in_date, $request->clock_in_time, $request->clock_out_date, $request->clock_out_time)) {
                return response()->json($result);
            }
            $clockedOutTimeStamp = $request->clock_out_date . ' ' . Carbon::parse($request->clock_out_time)->format('H:i');  //set format into timestamp
            $clock_out= $clockedOutTimeStamp;

        }else{
            $clock_out= $clockedInTimeStamp;
        }


        $formData = [

            'clock_in' => $clockedInTimeStamp,
            'clock_out'=>$clock_out,
            'user_id' => $request->user_id,
            'date' => $request->clock_in_date,

        ];


        //        update TimeSheet

        $result = Clock::find($id)->update($formData);
        if ($result) {
            return ['success' => 'Update the  Clock'];
        } else {
            return ['error' => ' Cannot update the  TimeSheet'];
        }
    }

    public function delete($id)
    {
        $result = Clock::find($id)->delete();
        if ($result) {
            return ['success' => 'Delete the  TimeSheet'];
        } else {
            return ['error' => ' Cannot Delete the  TimeSheet'];
        }
    }


    public function clockFormValidation($clock_in_date, $clock_in_time, $clock_out_date, $clock_out_time)
    {

//            check the clock_in and clocked out validation
        if ($clock_out_date != $clock_in_date) {
            return ['validation_errors' => ['Clock In and Clocked Out Date Must be Same']];
        } else if (Carbon::parse($clock_out_time)->format('H:i') < Carbon::parse($clock_in_time)->format('H:i')) {
            return ['validation_errors' => ['Clock  Out Time Must be Greater Than Clock In Time']];
        }


    }

}
