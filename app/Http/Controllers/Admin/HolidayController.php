<?php

namespace App\Http\Controllers\Admin;

use App\Clock;
use App\Holiday;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class HolidayController extends Controller
{
    private $data;



    public function __construct()
    {
        $this->data['title'] = 'Holidays';


    }

    public function index()
    {

        return view('admin.holiday.list', $this->data);
    }
    public function getHolidays()
    {


        return Holiday::with('User')->get();

    }

    public function store(Request $request)
    {


        $validator = Validator::make($request->all(),[
            'user_id' => 'required',
            'date' => 'required|unique:holidays,date',
            'note' => 'required',
            'type'=>'required'


        ],[
            'user_id.required' => ' Employee Field is required',
            'date.unique' => 'Date ('.$request->date.') had already taken on selected user',


        ]);
        if ($validator->fails()) {
            return response()->json(['validation_errors' => $validator->errors()]);
        }

//       check the clock in or not
        if($result=$this->checkClockIn($request->user_id,$request->date)){
            return response()->json($result);
        }






        $result = Holiday::create($request->all());
        if ($result) {


            return ['success' => 'Create the  Holiday'];
        } else {
            return ['error' => ' Cannot Create the  Holiday'];
        }
    }
    public function update(Request $request,$id)

    {


        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'date' => 'required|unique:holidays,date,'.$id,
            'note' => 'required',
            'type'=>'required'


        ],[
            'user_id.required' => ' Employee Field is required',
            'date.unique' => 'Date ('.$request->date.') had already taken on selected User',


        ]);
        if ($validator->fails()) {
            return response()->json(['validation_errors' => $validator->errors()]);
        }
        //       check the clock in or not
        if($result=$this->checkClockIn($request->user_id,$request->date)){
            return response()->json($result);
        }


        $result = Holiday::find($id)->update($request->all());
        if ($result) {


            return ['success' => 'Update the  Holiday'];
        } else {
            return ['error' => ' Cannot Update the  Holiday'];
        }
    }
    public function edit($id)
    {
        return Holiday::find($id);
    }
    public function delete($id)
    {
        $result = Holiday::find($id)->delete();
        if ($result) {
            return ['success' => 'Delete the  Holiday'];
        } else {
            return ['error' => ' Cannot Delete the  Holiday'];
        }
    }

    public  function  checkClockIn($user_id,$date){
        $clock_in=Clock::where(['user_id'=>$user_id])->whereDate('date',$date)->first();
        if($clock_in){
            return ['validation_errors' => ['Cannot Set Holiday, In Date ' .$date.' Selected User is present']];
        }else{
            return false;
        }


    }

}
