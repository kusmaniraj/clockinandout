<?php

namespace App\Http\Controllers;

use App\Clock;
use App\Holiday;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Validator;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\Traits\ClockValidation;

class UserController extends Controller
{
    protected $data;
    use ClockValidation;

    public function index()
    {

        $this->data['title'] = 'User Clock Time';



        return view('user.home', $this->data);
    }

    public function dashboard()
    {
        return view('user/dashboard');
    }

    public function storeClock(Request $request)
    {

//validation
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();
        }

//        check email
        $user = User::where(['email' => $request->email])->first();
        if ($user == false) {
            $validator->errors()->add('email', ' Invalid Email');
            return redirect()->back()->withErrors($validator)->withInput();
        }


//check password match
        if (Hash::check($request->password, $user->password) == false) {
            $validator->errors()->add('password', ' Invalid Password');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        //        check holidays or not from trait
        if ($result = $this->checkHoliday($user->id,Carbon::now()->format('Y-m-d'))) {
            $message='Cannot Clock In , Admin had set Holiday Today Date :' . Carbon::now()->format('Y-m-d');

            Session::flash('validation-error',$message);

            return redirect()->back()->withInput();

        }


        $clock_data ['user_id']= $user->id;
        $clock_data['date']=Carbon::now()->format('Y-m-d');


        $previousClocked = Clock::where(['user_id' =>$clock_data['user_id']])->whereDate('date', $clock_data['date'])->orderBy('id', 'desc')->first();



        if ($request->clock_status == 'clock_in') {



            //        check the previous clocked out date
            if($this->checkClockOutDate($user->id,$clock_data['date'])){
                Session::flash('validation-error', 'You had not clocked Out yet');
                return redirect()->back()->withInput();
            }


            //        check the previous clocked In Time
            if($this->checkClockOutTime($user->id,  $clock_data['date'])){
                Session::flash('validation-error', 'Admin had Already Set the Clocked out Time and Cannot Clock In  within the Clocked Out Time');
                return redirect()->back()->withInput();
            }
            $clock_data['clock_in']  =Carbon::now()->format('Y-m-d H:i:d');
            $clock_data['clock_out']  =Carbon::now()->format('Y-m-d H:i:d');
            $this->clockIn($clock_data);


        } else {


            //        check the previous clocked In date
           if($this->checkClockInDate($user->id,  $clock_data['date'])){
               Session::flash('validation-error', 'You had not clocked In or Admin  had already Clocked Out');
               return redirect()->back()->withInput();
           }



            $clock_data['clock_out']  =Carbon::now()->format('Y-m-d H:i:d');
            $this->clockOut($previousClocked['id'], $clock_data['user_id'], $clock_data);


        }
        return redirect()->back();

    }

    public function clockIn($data)
    {
        $result = Clock::create($data);
        if ($result) {

            Session::flash('success', 'Successfully Clock In');
        } else {
            Session::flash('error', 'Error to  Clock In');
        }
    }

    public function clockOut($id, $user_id, $data)
    {

        $result = Clock::where(['id' => $id, 'user_id' => $user_id])->update($data);
        if ($result) {
            Session::flash('success', 'Successfully Clock Out');
        } else {
            Session::flash('error', 'Error to  Clock Out');
        }
    }




}
