<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;
use Config;
use App\Clock;

class SessionDataCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        $bag = Session::all();


        $max = config('session.lifetime');// min to hours conversion

        if (($bag && $max < (time() - $bag->getLastUsed()))) {


            $user_id = Auth::guard('web')->user()->id;

            $clocked_in_user = Clock::with('User')->where(['user_id' => $user_id, 'clock_out' => null])->first();


            Clock::find($user_id)->updated(['clock_our' => date('Y-m-d H:i', strtotime('+9 hour', strtotime($clocked_in_user->clock_in)))]);
            $request->session()->flush(); // remove all the session data
            Auth::guest('web')->logout(); // logout user

        }
        return $next($request);
    }
}
