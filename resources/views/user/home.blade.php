@extends('layouts.auth')




@push('styles')
<style>
    body.auth-page{
        background-color: #000;

        z-index: -1;
    }
    body.auth-page::before{
        position: absolute;
        background-image: url("{{asset('images/time-clock.jpg')}}");
        z-index: -1;
        content:' ';
        display: block;
        height: 100%;
        width: 100%;
        opacity: 0.5;
    }
    .auth-page .box{
        border: none;
    }



    .clockdate-wrapper {
        background-color: #00c0ef;
        padding: 25px;

        width: 100%;
        text-align: center;
        border-radius: 10px;
        margin: 0 auto;

    }

    #clock {
        background-color: #00c0ef;
        font-family: sans-serif;
        font-size: 60px;
        text-shadow: 0px 0px 10px #fff;
        color: #000;
    }

    #clock span {
        color: #fff;
        text-shadow: 0px 0px 4px #333;
        font-size: 30px;
        position: relative;
        top: -27px;
        left: -10px;
    }

    #date {
        letter-spacing: 10px;
        font-size: 16px;
        font-family: arial, sans-serif;
        text-shadow: 0px 0px 10px #333;
        color: #fff;
    }
    .clocked_in_time{
        background-color: #00c0ef;
        font-family: sans-serif;
        padding: 0px 8px;
      border: 2px solid  #00c0ef;
        border-radius: 5px;
        color:#fff;

    }
</style>
@endpush
@section('content')

<!--main content-->
<section class="content">
    <div class="container justify-content-center">
        <!-- Default box -->
        <div class="box ">
            <div class="login-logo">
                <a href="#"><b>{{config('setting.company_name')}} </b></a>
                <hr>
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <div id="clockdate">
                        <div class="clockdate-wrapper">
                            <div id="clock"></div>
                            <div id="date"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <form action="{{route('user.storeClock')}}" method="post">
                        @include('message.alertMessage')
                        @csrf


                        <div class="row form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-3">
                                <label for="email">Email</label>
                            </div>
                            <div class="col-md-9">
                                <input type="email" class="form-control" name="email" placeholder="Input Email"
                                       value="{{old('email')}}">
                                @if ($errors->has('email'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                        <div class="row form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-md-3">
                                <label for="password">Password</label>
                            </div>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="password"
                                       placeholder="Input Password">
                                @if ($errors->has('password'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                        <div class="row form-group">

                            <div class="col-md-offset-3 col-md-9">


                                <button type="submit" class="btn btn-success" name="clock_status" value="clock_in">
                                    Clock In
                                </button>
                                <button type="submit" class="btn btn-danger" name="clock_status" value="clock_out">
                                    Clock Out
                                </button>

                                <!--                            <button type="submit" class="btn btn-primary">Login In </button>-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                {{config("setting.company_name")}} ©{{date('Y',strtotime(config("setting.company_start_date")))}}-{{now()->format('Y')}} . All rights reserved.
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->
    </div>


</section>
<!--emd of main content-->


@endsection
@push('scripts')
<script type="text/javascript">
    window.onload = startTime();

    function startTime() {
        var today = new Date();
        var hr = today.getHours();
        var min = today.getMinutes();
        var sec = today.getSeconds();
        ap = (hr < 12) ? "<span>AM</span>" : "<span>PM</span>";
        hr = (hr == 0) ? 12 : hr;
        hr = (hr > 12) ? hr - 12 : hr;
        //Add a zero in front of numbers<10
        hr = checkTime(hr);
        min = checkTime(min);
        sec = checkTime(sec);
        document.getElementById("clock").innerHTML = hr + ":" + min + ":" + sec + " " + ap;

        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        var curWeekDay = days[today.getDay()];
        var curDay = today.getDate();
        var curMonth = months[today.getMonth()];
        var curYear = today.getFullYear();
        var date = curWeekDay + ", " + curDay + " " + curMonth + " " + curYear;
        document.getElementById("date").innerHTML = date;

        var time = setTimeout(function () {
            startTime()
        }, 500);
    }
    function checkTime(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
</script>
@endpush
