<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('AdminLTE/dist/img/avatar.png')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ucfirst(Auth::user()->name )}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="@isset($title){{$title=='Dashboard' ? 'active': ''}} @endisset"><a
                    href="{{route('admin.dashboard')}}"><i class="fa  fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="@isset($title){{$title=='Employee' ? 'active': ''}} @endisset"><a
                    href="{{route('employee.index')}}"><i class="fa  fa-users"></i> <span>Employee</span></a></li>
            <li class="@isset($title){{$title=='TimeSheet' ? 'active': ''}} @endisset"><a
                    href="{{route('timeSheet.index')}}"><i class="fa  fa-clock-o"></i> <span>TimeSheet</span></a></li>
            <li class="@isset($title){{$title=='Reports' ? 'active': ''}} @endisset"><a
                    href="{{route('report.index')}}"><i class="fa  fa-file-text-o"></i> <span>Reports</span></a></li>
            <li class="@isset($title){{$title=='Holidays' ? 'active': ''}} @endisset"><a
                    href="{{route('holiday.index')}}"><i class="fa  fa-calendar-check-o"></i> <span>Holidays</span></a></li>
            <li class="@isset($title){{$title=='Setting' ? 'active': ''}} @endisset"><a
                    href="{{route('setting.index')}}"><i class="fa  fa-gear"></i> <span>Setting</span></a></li>


    </section>
    <!-- /.sidebar -->
</aside>
