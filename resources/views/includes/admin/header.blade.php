<header class="main-header">
    <!-- Logo -->
    <a href="{{asset('AdminLTE/index2.html')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>{{config('setting.company_short_name')}} CA</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>{{config('setting.company_name')}} </b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success"> @isset($messages) {{count($messages)}} @endisset</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have @isset($messages) {{count($messages)}} @endisset messages</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                @isset($messages)
                                @foreach($messages as $key=>$message)
                                <li><!-- start message -->
                                    <a href="{{route('message.index')}}">
                                        <div class="pull-left">
                                            {{$message->name}}
                                        </div>
                                        <h4>
                                           {{$message->email}}
                                            <small><i class="fa fa-clock-o"></i>      {{$message->created_at->diffForHumans()}}</small>
                                        </h4>
                                        <p> {{$message->message}}</p>
                                    </a>
                                </li>
                                @endforeach
                                @endisset
                                <!-- end message -->
                            </ul>
                        </li>
                        <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{asset('AdminLTE/dist/img/avatar.png')}}" class="user-image" alt="User Image">
                        <span class="hidden-xs">   {{ucfirst(Auth::user()->name)}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{asset('AdminLTE/dist/img/avatar.png')}}" class="img-circle" alt="User Image">

                            <p>
                             {{ucfirst(Auth::user()->name)}}

                            </p>
                        </li>
                        <!-- Menu Body -->

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{route('admin.profile')}}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <form action="{{route('admin.logout')}}" method="post">
                                {{csrf_field()}}
                                <button type="submit"  class="btn btn-default btn-flat">Sign out</button>
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</header>
