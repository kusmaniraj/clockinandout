@if(Session::has('success'))
<div class="alert alert-success alert-msg">
    <strong>Successfully !!</strong>&nbsp;&nbsp;{{Session::get('success')}}
</div>
@endif
@if(Session::has('error'))
<div class="alert alert-danger alert-msg">
    <strong>Error !!</strong>&nbsp;&nbsp;{{Session::get('error')}}
</div>
@endif
@if(Session::has('validation-error'))
<div class="alert alert-danger ">
    <strong>Oops !!</strong>&nbsp;&nbsp;{{Session::get('validation-error')}}
</div>
@endif


@push('scripts')
<script>
    window.setTimeout(function() {
        $('.alert-msg').slideUp(1000,function(){
            $(this).remove();
        });
    },5000)

</script>
@endpush
