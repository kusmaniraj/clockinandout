@extends('layouts.admin')

@push('styles')
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/bootstrap-timepicker/bootstrap-timepicker.min.css')}}">

<style>
    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }
    tr.child{
        background-color: #f1f1f1;
    }
    tr.shown{
        background-color: #ccc;
    }
    td span{
        margin: 5px 10px;
    }
</style>
@endpush
@section('content')


<!-- Main content -->
<section class="content " id="page_report">

    <!--  box -->
    <div class="box box-primary" id="report_page">


        <div class="box-body">


            <div class="box-header with-border">
                <h3 class="box-title"><strong> Report Search</strong></h3>


            </div>
            <div class="box-body">
                <!--    report tabs-->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Timeline</a></li>
                        <li><a href="#tab_2" data-toggle="tab">Monthly Schedule</a></li>
                        <li><a href="#tab_3" data-toggle="tab">Weekly Schedule  </a></li>


                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <form id="timeline_report_form">
                                <input type="hidden" name="reportType" value="timeline">

                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <label for="date">Select Employee</label>

                                        <select name="user_id" id="" class="form-control">
                                            <option value="0" disabled selected>Select Employee</option>
                                        </select>

                                    </div>
                                    <div class="col-md-4">
                                        <label for="start_date">Start Date</label>

                                        <input type="text" name="start_date" class=" datepicker form-control pull-right"
                                               id=""
                                               data-date-format='yyyy-mm-dd' value="" placeholder="Input Start Date">

                                    </div>
                                    <div class="col-md-4">
                                        <label for="date">End Date</label>

                                        <input type="text" name="end_date" class=" datepicker form-control pull-right"
                                               id=""
                                               data-date-format='yyyy-mm-dd' value="" placeholder="Input End Date">

                                    </div>


                                </div>


                                <div class="row form-group">

                                    <div class=" col-md-9">
                                        <button type="submit" class="btn btn-primary" name="timeline">View Report
                                        </button>


                                    </div>

                                </div>

                            </form>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <form id="monthly_report_form">

                                <input type="hidden" name="reportType" value="monthly">

                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <label for="date">Select Employee</label>

                                        <select name="user_id" id="" class="form-control">
                                            <option value="0" disabled selected>Select Employee</option>
                                        </select>

                                    </div>
                                    <div class="col-md-4">
                                        <label for="year">Year</label>

                                        <input type="text" name="year" class="yearPicker  form-control pull-right"
                                               value="{{ \Carbon\Carbon::now()->format('Y')}}">

                                    </div>
                                    <div class="col-md-4">
                                        <label for="month">Month</label>

                                        <input type="text" name="month" class="monthPicker  form-control pull-right"

                                               value="{{ \Carbon\Carbon::now()->subDays(30)->format('m')}}">

                                    </div>


                                </div>


                                <div class="row form-group">

                                    <div class=" col-md-9">
                                        <button type="submit" class="btn btn-primary" name="monthly">View Report
                                        </button>


                                    </div>

                                </div>

                            </form>
                        </div>
                        <div class="tab-pane" id="tab_3">
                            <form id="weekly_report_form">

                                <input type="hidden" name="reportType" value="weekly">

                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <label for="date">Select Employee</label>

                                        <select name="user_id" id="" class="form-control">
                                            <option value="0" disabled selected>Select Employee</option>
                                        </select>

                                    </div>
                                    <div class="col-md-3">
                                        <label for="year">Year</label>

                                        <input type="text" name="year" class="  yearPicker form-control pull-right"
                                               value="{{ \Carbon\Carbon::now()->format('Y')}}">

                                    </div>
                                    <div class="col-md-3">
                                        <label for="month">Month</label>

                                        <input type="text" name="month" class="monthPicker  form-control pull-right"

                                               value="">

                                    </div>
                                    <div class="col-md-3">
                                        <label for="month">Week</label>

                                        <select name="week" id="week" class="form-control">
                                            <option value="1" selected disabled>- </option>
                                        </select>

                                    </div>


                                </div>


                                <div class="row form-group">

                                    <div class=" col-md-9">
                                        <button type="submit" class="btn btn-primary" name="monthly">View Report
                                        </button>


                                    </div>

                                </div>

                            </form>
                        </div>
                        <!-- /.tab-pane -->

                    </div>
                    <!-- /.tab-content -->
                </div>
                <!--    end of report tabs-->

            </div>


        </div>
        <!-- /.box-body -->


    </div>

    <!--  box -->
    <div class="box box-primary">


        <div class="box-header with-border">
            <h3 class="box-title"><strong>EMPLOYEE TIMESHEET STANDARD REPORT</strong></h3>
            <a href="#" id="generate_pdf" class="btn btn-primary pull-right">Export <i class="fa fa-file-pdf-o"></i></a>


        </div>
        <div class="box-body" id="employee-report">


            <table id="report_table" class="table">
                <thead>
                <tr>
                    <th style="width: 20px">&nbsp;</th>
                    <th>Date</th>
                    <th>Holiday</th>
                    <th>Working Hour</th>



                </tr>
                </thead>
                <tbody>


                </tbody>
                <tfoot>
                <tr>

                    <th colspan="3"><p style="float: right">Total Hours</p></th>
                    <th class="total_hrs"></th>
                </tr>
                </tfoot>


            </table>


        </div>


        <!-- /.box-body -->


    </div>


    <!--    pdf report table-->
    <div id="employee-pdf-report" style="display: none">


        <table id="pdf_report_table" class="table">
            <thead>
            <tr>
                <th style="width: 20px">&nbsp;</th>
                <th>Date</th>
                <th>Holiday</th>
                <th>Working Hour</th>



            </tr>
            </thead>
            <tbody>


            </tbody>
            <tfoot>
            <tr>

                <th colspan="3"><p style="float: right">Total Hours</p></th>
                <th class="total_hrs"></th>
            </tr>
            </tfoot>


        </table>


    </div>

</section>


@endsection

@push('scripts')


<!--datatable js-->
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js"></script>


<!--date-->
<script type="text/javascript" src="{{asset('AdminLTE/plugins/date.js')}}"></script>


<script src="{{asset('build/pages/admin/shared/shared.js')}}"></script>
<script src="{{asset('build/pages/admin/report.js')}}"></script>
<!--date picker-->
<script src="{{asset('AdminLTE/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<!--time picker-->
<script src="{{asset('AdminLTE/bower_components/bootstrap-timepicker/bootstrap-timepicker.min.js')}}"></script>

<!--for Pdf create-->
<script type="text/javascript" src="{{asset('AdminLTE/dist/js/jspdf.min.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminLTE/dist/js/html2canvas.min.js')}}"></script>

<script>


    //    datepicker
    $('.datepicker').datepicker({
        autoclose: true,
        dateFormat: 'yyyy-mm-dd',
        todayHighlight: true,
        endDate: 'today',
    })

    //    yearPpicker
    $('.yearPicker').datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years",
        endDate: 'today',
        setDate: 'today',
        todayHighlight: true,
    })

    //    monthPicker
    $('.monthPicker').datepicker({
        format: "mm",
        viewMode: "months",
        minViewMode: "months",
        todayHighlight: true,
    })


</script>


@endpush


