<div class="modal" id="holiday_modal_form" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Holiday Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="holiday_form">
                    <input type="hidden" name="id">

                    <div class="row form-group">
                        <div class="col-md-3">
                            <label for="user_id">Employee Name<span class="required text-danger">*</span></label>
                        </div>
                        <div class="col-md-9">

                            <select name="user_id" id="" class="form-control">
                                <option value="0" disabled>Select Employee</option>

                            </select>
                        </div>

                    </div>
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label for="type">Holiday Type<span class="required text-danger">*</span></label>
                        </div>
                        <div class="col-md-9">

                            <select name="type" id="" class="form-control">
                                <option value="0" disabled>Select Holiday Type</option>
                                <option>Leave</option>
                                <option>Sick</option>

                            </select>
                        </div>

                    </div>

                    <div class="row form-group">


                        <div class="col-md-3">
                            <label for="date">Holiday Date<span class="required text-danger">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group has-feedback">



                                <input type="text" name="date" class=" datepicker form-control pull-right" id=""
                                       data-date-format='yyyy-mm-dd' value="">
                                <span class="fa fa-calendar form-control-feedback"></span>
                            </div>


                        </div>


                    </div>

                    <div class="row form-group">


                        <div class="col-md-3">
                            <label for="note">Note <span class="required text-danger">*</span></label>
                        </div>
                        <div class="col-md-9">


                            <textarea name="note" id="" cols="30" rows="10" class="form-control"></textarea>

                        </div>


                    </div>


                    <div class="row form-group">

                        <div class=" col-md-9">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>

                </form>

            </div>

        </div>
    </div>
</div>

@push('scripts')


@endpush
