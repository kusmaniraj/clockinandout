@extends('layouts.admin')

@push('styles')
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/bootstrap-timepicker/bootstrap-timepicker.min.css')}}">
@endpush
@section('content')


<!-- Main content -->
<section class="content " id="page_holiday">


    <div class="box box-primary">


        <div class="box-body">


            <div class="box-header with-border">
                <h3 class="box-title"><strong>Employee List Holiday</strong></h3>
                <a href="#" id="add_holiday_btn"  class="btn btn-primary pull-right">Add Holiday</a>

            </div>
            <div class="box-body">
                <table id="holiday_table" class="table">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Holiday Date</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>


                    </tbody>


                </table>
            </div>


        </div>
        <!-- /.box-body -->


    </div>


</section>

@include('admin.holiday.formModal')
@endsection

@push('scripts')



<!--datatable js-->
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>


<!--date-->
<script type="text/javascript" src="{{asset('AdminLTE/plugins/date.js')}}"></script>


<script src="{{asset('build/pages/admin/shared/shared.js')}}"></script>
<script src="{{asset('build/pages/admin/holiday.js')}}"></script>
<!--date picker-->
<script src="{{asset('AdminLTE/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>



<script>

    //    datepicker
    $('.datepicker').datepicker({
        autoclose: true,
        dateFormat: 'yyyy-mm-dd',
        todayHighlight: true,

    })



</script>

@endpush


