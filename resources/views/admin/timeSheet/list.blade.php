@extends('layouts.admin')

@push('styles')
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/bootstrap-timepicker/bootstrap-timepicker.min.css')}}">
@endpush
@section('content')


<!-- Main content -->
<section class="content " id="page_timeSheet">

    <!--  box -->
    <div class="box box-primary" id="timeSheet_page">


        <div class="box-body">


            <div class="box-header with-border">
                <h3 class="box-title"><strong> TimeSheet Search</strong></h3>


            </div>
            <div class="box-body">
                <form id="timeSheet_search_form">


                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="date">Select Employee</label>

                            <select name="user_id" id="" class="form-control">
                                <option value="0">All</option>
                            </select>

                        </div>
                        <div class="col-md-4">
                            <label for="start_date">Start Date</label>

                                <input type="text" name="start_date" class=" search-datepicker form-control pull-right" id=""  data-date-format='yyyy-mm-dd' value="" placeholder="Input Start Date">

                        </div>
                        <div class="col-md-4">
                            <label for="date">End Date</label>

                            <input type="end_date" name="end_date" class=" search-datepicker form-control pull-right" id=""  data-date-format='yyyy-mm-dd' value="" placeholder="Input End Date">

                        </div>



                    </div>








                    <div class="row form-group">

                        <div class=" col-md-9">
                            <button type="submit" class="btn btn-primary">Search <i class="fa fa-search"></i></button>
                            <a href="#" id="refresh_btn" class="btn btn-default">Refresh <i class="fa fa-refresh"></i></a>


                        </div>

                    </div>

                </form>
            </div>


        </div>
        <!-- /.box-body -->


    </div>

    <!--  box -->
    <div class="box box-primary">


        <div class="box-body">


            <div class="box-header with-border">
                <h3 class="box-title"><strong>Employee List TimeSheet</strong></h3>
                <a href="#" id="add_timeSheet_btn"  class="btn btn-primary pull-right">Add TimeSheet</a>

            </div>
            <div class="box-body">
                <table id="timeSheet_table" class="table">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Date</th>
                        <th>Name</th>

                        <th>Clocked In</th>

                        <th>Clocked Out</th>
                        <th>Working Hour(Durations)</th>


                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>


                    </tbody>


                </table>
            </div>


        </div>
        <!-- /.box-body -->


    </div>


</section>

@include('admin.timeSheet.formModal')
@endsection

@push('scripts')



<!--datatable js-->
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>


<!--date-->
<script type="text/javascript" src="{{asset('AdminLTE/plugins/date.js')}}"></script>


<script src="{{asset('build/pages/admin/shared/shared.js')}}"></script>
<script src="{{asset('build/pages/admin/timeSheet.js')}}"></script>
<!--date picker-->
<script src="{{asset('AdminLTE/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<!--time picker-->
<script src="{{asset('AdminLTE/bower_components/bootstrap-timepicker/bootstrap-timepicker.min.js')}}"></script>


<script>

    //    datepicker
    $('.datepicker').datepicker({
        autoclose: true,
        dateFormat: 'yyyy-mm-dd',
        todayHighlight: true,

    })
    $('.search-datepicker').datepicker({
        autoclose: true,
        dateFormat: 'yyyy-mm-dd',
        todayHighlight: true,
        endDate: 'today',
    })


    //Timepicker
    $('.timepicker').timepicker({
        showInputs: false
    })

</script>

@endpush


