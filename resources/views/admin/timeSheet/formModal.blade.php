<div class="modal" id="timeSheet_modal_form" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add TimeSheet Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="timeSheet_form">
                    <input type="hidden" name="id">

                    <div class="row form-group">
                        <div class="col-md-3">
                            <label for="user_id">Employee Name<span class="required text-danger">*</span></label>
                        </div>
                        <div class="col-md-9">

                            <select name="user_id" id="" class="form-control">
                                <option value="0" disabled>Select Employee</option>

                            </select>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6 ">
                            <div class="form-group has-feedback">
                                <label for="clock_in">Clocked In <span class="required text-danger">*</span></label>

                                <input type="text" name="clock_in_date" class=" datepicker form-control"
                                       id="" data-date-format='yyyy-mm-dd' value="{{now()->toDateString()}}">
                                <span class="fa fa-calendar form-control-feedback"></span>
                            </div>

                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group has-feedback">
                                <label for="clock_in_time">At<span class="required text-danger">*</span></label>

                                <input type="text" class="form-control timepicker" name="clock_in_time"
                                       value="{{now()->format('h:i A')}}">
                                <span class="fa fa-clock-o form-control-feedback"></span>
                            </div>

                        </div>

                    </div>
                    <div class="row ">

                        <div class="col-md-6 ">
                            <div class="form-group has-feedback">
                                <label for="clock_in">Clocked Out </label>


                                <input type="text" name="clock_out_date" class=" datepicker form-control " id=""
                                       data-date-format='yyyy-mm-dd' value="{{now()->toDateString()}}">
                                <span class="fa fa-calendar form-control-feedback"></span>
                            </div>


                        </div>
                        <div class="col-md-6">
                            <div class=" form-group has-feedback">
                                <label for="clock_in_time">At</label>

                                <input type="text" class="form-control timepicker" name="clock_out_time">
                                <span class="fa fa-clock-o form-control-feedback"></span>
                            </div>

                        </div>

                    </div>


                    <div class="row form-group">

                        <div class=" col-md-9">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>

                </form>

            </div>

        </div>
    </div>
</div>

@push('scripts')


@endpush
