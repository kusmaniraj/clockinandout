@extends('layouts.admin')
@isset($getSetting)

@php
$name=$getSetting->company_name;
$short_name=$getSetting->company_short_name;
$slogan=$getSetting->company_slogan;
$version=$getSetting->company_version;
$start_date=$getSetting->company_start_date;

$email=$getSetting->email;
$address=$getSetting->address;
$phone=$getSetting->phone_number;
$fax=$getSetting->fax_number;


$route=route('setting.store',$getSetting->id);
$action='Update';


@endphp

@else
@php
$name=old('company_name');
$short_name=old('company_short_name');
$slogan=old('company_slogan');
$version=old('company_version');
$start_date=old('company_start_date');

$email=old('email');
$address=old('address');
$phone=old('phone_number');
$fax=old('fax_number');

$route=route('setting.store');
$action='Create';
$method
@endphp
@endisset
@section('content')



<!-- Main content -->
<section class="content">
    <form role="form" method="post" action="{{$route}}">
        <!--        alert message-->
        @include('message.alertMessage')
        {{csrf_field()}}
        <!--  box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Company Info</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('company_name') ? ' has-error' : '' }} ">
                            <label for="companyName">Company Name &nbsp;<span
                                    class="required text-danger">*</span></label>
                            <input type="text" class="form-control " id="companyName" placeholder="Enter Company Name"
                                   name="company_name" value="{{$name}}" >
                            @if ($errors->has('company_name'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('company_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('company_short_name') ? ' has-error' : '' }}">
                            <label for="companyShortName">Company Short Name&nbsp;<span
                                    class="required text-danger">*</span></label>
                            <input type="text" class="form-control" id="companyShortName"
                                   placeholder="Enter Company Short Name" name="company_short_name" value="{{$short_name}}">
                            @if ($errors->has('company_short_name'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('company_short_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('company_slogan') ? ' has-error' : '' }}">
                            <label for="companySlogan">Company Slogan</label>
                            <input type="text" class="form-control" id="companySlogan"
                                   placeholder="Enter Company Slogan" name="company_slogan" value="{{$slogan}}">
                            @if ($errors->has('company_slogan'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('company_slogan') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">Email &nbsp;<span class="required text-danger">*</span></label>
                            <input type="email" class="form-control" id="email" placeholder="Enter Company Email"
                                   name="email" value="{{$email}}">
                            @if ($errors->has('email'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="">Address&nbsp;<span class="required text-danger">*</span></label>
                            <input type="text" class="form-control" id="companyAddress"
                                   placeholder="Enter Company Address" name="address" value="{{$address}}">
                            @if ($errors->has('address'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('address') }}</strong>
                                    </span>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('phone_number') ? ' has-error' : '' }}">
                            <label for="companyPhoneNo">Phone Number&nbsp;<span
                                    class="required text-danger">*</span></label>
                            <input type="number" class="form-control" id="companyPhoneNo"
                                   placeholder="Enter Company Phone Number" name="phone_number" value="{{$phone}}">

                            @if ($errors->has('email'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('phone_number') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group ">
                            <label for="companyFaxNo">Fax&nbsp;<span class="required text-danger">*</span></label>
                            <input type="number" class="form-control" id="companyFax" placeholder="Enter CompanyFax"
                                   name="fax_number" value="{{$fax}}">
                        </div>
                    </div>
                </div>


            </div>
            <!-- /.box-body -->


        </div>





        <div class="box ">
            <div class="box-body ">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('company_version') ? ' has-error' : '' }}">
                            <label for="exampleInputFile">Company Version</label>
                            <input type="number" name="company_version" class="form-control" value="{{$version}}">
                            @if ($errors->has('company_version'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('company_version') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('company_start_date') ? ' has-error' : '' }}">
                            <label for="exampleInputFile">Company Start date</label>
                            <input type="date" name="company_start_date" class="form-control" value="{{$start_date}}">
                            @if ($errors->has('company_start_date'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('company_start_date') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">{{$action}}</button>
            </div>
        </div>
    </form>
</section>
<!--main content-->

@endsection
@push('scripts')

@endpush

