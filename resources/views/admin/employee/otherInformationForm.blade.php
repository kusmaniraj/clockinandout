<div id="other_information_page" style="display: none">
    <h3 class="box-title">Other Information</h3>
    <div class="row">
        <div class="col-md-4">

            <div class="form-group has-feedback ">
                <label for="department">Department <span class="text-danger">*</span></label>
                <select name="department" id="department" class="form-control">
                    <option value="0" selected disabled>Select Department</option>
                    <option value="web_development">Web Development</option>
                </select>


            </div>
        </div>


    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group has-feedback">
                <label for="country">Country <span class="text-danger">*</span></label>
                <select name="country" id="country" class="form-control">
                    <option value="0" selected disabled>Select Country</option>

                </select>

            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group has-feedback">
                <label for="state">State <span class="text-danger">*</span></label>
                <select name="state" id="state" class="form-control">
                    <option selected disabled>Select State</option>
                </select>

            </div>
        </div>



    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group has-feedback">
                <label for="city">City <span class="text-danger">*</span></label>
                <input type="text" class="form-control" placeholder="City" value=""
                       name="city">
                <span class="fa fa-map-marker form-control-feedback"></span>

            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group has-feedback">
                <label for="street">Street <span class="text-danger">*</span></label>
                <input type="text" class="form-control" placeholder="Street" value=""
                       name="street">
                <span class="fa fa-map-marker form-control-feedback"></span>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group has-feedback ">
                <label for="phone">Phone <span class="text-danger">*</span></label>
                <input type="text" class="form-control" placeholder="Phone" value=""
                       name="phone">
                <span class="fa fa-phone form-control-feedback"></span>

            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group has-feedback">
                <label for="fax">Fax</label>
                <input type="text" class="form-control" placeholder="fax" value=""
                       name="fax">
                <span class="fa fa-fax form-control-feedback"></span>

            </div>
        </div>
    </div>

</div>
