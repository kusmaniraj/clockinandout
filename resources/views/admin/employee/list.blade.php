@extends('layouts.admin')

@section('content')


<!-- Main content -->
<section class="content" id="employee">
    <div class="row">
        <div class="col-md-3 list-employees">


            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Employee List </h3><span class="pull-right"><a href="#" id="addEmployeeBtn"
                                                                                         data-toggle="tooltip"
                                                                                         title="Add Employee"><i
                                class="fa fa-user-plus" style="font-size: 18px"></i></a></span>


                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked" id="employee_list">
                    </ul>


                    <div class="contextMenu  hidden" id="employee-action">
                        <ul>
                            <li id="edit"><i class="fa fa-edit"></i> &nbsp;Edit</li>

                            <li id="delete"><i class="fa fa-trash"></i> &nbsp; Delete</li>
                            <li id="reset_password"><i class="fa fa-lock"></i> &nbsp; Reset Password</li>

                        </ul>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <!-- /.col -->
        <div class="col-md-9 employee-form-page">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title action"><strong>Add Employee</strong></h3>


                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <form method="POST" id="employee_form">
                        <!--                        alert messages-->
                        @include('message.alertMessage')

                        <input type="hidden" name="id">

                        <div class="form-group has-feedback " id="name">
                            <input type="text" class="form-control" placeholder="Name" value=""
                                   name="name">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>

                        </div>
                        <div class="form-group has-feedback " id="email">
                            <input type="email" class="form-control" placeholder="Email" value=""
                                   name="email">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                        </div>


                        <div class="form-group has-feedback " id="password">
                            <input type="password" class="form-control" placeholder="Password" name="password">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                        </div>
                        <div class="form-group has-feedback " id="password_confirmation">
                            <input type="password" class="form-control" placeholder="Password Confirmation"
                                   name="password_confirmation">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                        </div>

<!--                      other Information Form-->
                        @include('admin.employee.otherInformationForm')


                        <div class="row">

                            <div class="col-xs-2">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Create</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>

                </div>

            </div>
            <!-- /. box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

</section>
<!--main content-->

@endsection
@push('scripts')

<script type="text/javascript"
        src="{{asset('AdminLTE/plugins/contextMenu/contextMenu.min.js')}}"></script>
<script src="{{asset('build/pages/admin/shared/shared.js')}}"></script>
<script src="{{asset('build/pages/admin/employee.js')}}"></script>
@endpush


